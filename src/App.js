import React from 'react';
import './App.css';
import Articles from './Articles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'

function App() {
  return (
    <div className="App">
    <AppBar position="static" color="default">
        <Toolbar>
          <Typography variant="h6" color="inherit">
            Shopping Application
          </Typography>
        </Toolbar>
      </AppBar>
      <header className="App-header">
        <Articles />
      </header>
    </div>
  );
}

export default App;
