import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';

const INITIAL_STATE = [
    {
        id: 1,
        nom: "Article 1",
        quantite: 0,
    },
    {
        id: 2,
        nom: "Article 2",
        quantite: 0,
    },
    {
        id: 3,
        nom: "Article 3",
        quantite: 0,
    }
];

export default class Articles extends Component {
    state = {
        articles: INITIAL_STATE,
    }
    addArticle = (index) => {
        const newArticles = [...this.state.articles];
        const item = newArticles[index].quantite;
        newArticles[index].quantite = item  + 1;
        this.setState({ articles: newArticles });
    }
    deletArticle = (id) => {
        const newList = this.state.articles.filter(item => item.id !== id);
        this.setState({ articles: newList });
    }
    refresh = () => {
        this.setState({ articles: INITIAL_STATE });
    }
    render() {
        return (
        <div>
            <button onClick={this.refresh}>Refresh</button>
            <Grid container>
                 <table>
                         <tr>
                            <th>Nom</th>
                            <th>Quantite</th>
                            <th>Actions</th>
                        </tr>
                        {this.state.articles.map((article, index) => (
                            <tr key={index}>
                                <td>{article.nom}</td>
                                <td>{article.quantite}</td>
                                <button onClick={() => this.addArticle(index)}><i className="fa fa-plus"></i></button>
                                <button onClick={() => this.deletArticle(article.id)}><i className="fa fa-trash"></i></button>
                            </tr>
                        ))}
                </table>
                </Grid>
                </div>
        )
    }
}
